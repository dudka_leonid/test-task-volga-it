package com.e.testtaskvolga_it.service

import android.database.Observable
import com.e.testtaskvolga_it.model.RepositoryMini
import com.e.testtaskvolga_it.model.RepositoryDetails
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubService {
    @GET("repositories/")
    fun getRepositoriesAsync() : Deferred<Response<List<RepositoryMini>>>

    @GET("repos/{owner}/{repo}")
    fun getRepositoryAsync(@Path("owner") owner: String,
                           @Path("repo") repo: String)
            : Deferred<Response<RepositoryDetails>>

    @GET("users/{owner}/repos")
    fun listRepositoriesAsync(@Path("owner") owner: String,
                              @Query("per_page") sort: String)
            : Deferred<Response<List<RepositoryMini>>>
}