package com.e.testtaskvolga_it.service

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object GitServiceFactory {

    const val testUsername = "Limfips"
    private const val testPassword = "+Ll2012sat2281"
    private const val fURL: String = "https://api.github.com/"
    private const val sURL: String = "https://$testUsername:$testPassword@api.github.com/"

    private fun retrofit() : Retrofit = Retrofit.Builder()
        .client(OkHttpClient().newBuilder().build())
        .baseUrl(sURL)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val gitHubApi : GitHubService = retrofit().create(GitHubService::class.java)
}