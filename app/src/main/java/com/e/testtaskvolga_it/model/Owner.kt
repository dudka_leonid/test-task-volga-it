package com.e.testtaskvolga_it.model

data class Owner(
    val login: String,
    val avatar_url: String
)