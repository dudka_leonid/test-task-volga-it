package com.e.testtaskvolga_it.model

data class RepositoryMini(
    val name: String,
    val description: String,
    val languageProgramming: String,
    val forks: Int,
    val stars: Int,
    val owner: Owner
)